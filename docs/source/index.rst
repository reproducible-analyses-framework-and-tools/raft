Welcome to RAFT's documentation!
===================================

**RAFT** (Reproducible Analyses Framework and Tools) is a Python wrapper for
Nextflow DSL2 that allows users to easily run common bioinformatics workflow
while providing complete control of workflow behaviors.

Check out the :doc:`usage` section for further information, including
how to :ref:`installation` the project.

.. note::

   This project is under active development.

Contents
--------

.. toctree::

   installation
   manifest
   usage
   outputs
   modules
   ots-workflows
