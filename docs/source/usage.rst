Usage
=====

Quick Start
___________

.. _demonstration workflow:

Users can run a demonstration workflow by running:

.. code-block:: console

  $ raft.py run-demo -w rna-alns-transcript-counts

This will:
  - Create a project named ``demo-rna-alns-transcript-counts`` in ``./raft/projects``
  - Download the required RAFT modules
  - Download the demo references, FASTQS, and manifest
  - Run the project's workflow

Outputs can be found in
``./raft/projects/demo-rna-alns-transcript-counts/outputs``. More information
about output directory organization can be found in :doc:`outputs`.

Finding Off-the-shelf Workflows
_______________________________

.. _available workflows:

RAFT supports a variety of workflows. Users can list the currently available
workflows by running:

.. code-block:: console

  $ raft.py available-workflows

Each workflow supports a variety of species (human or mouse) as well as a
variety of starting inputs. Users can find which species and inputs are
supported for each workflow by running:

.. code-block:: console

  $ raft.py available-workflows -w <WORKFLOW_NAME>

For example:

.. code-block:: console

  $ raft.py available-workflows -w gene-fusions

produces the output

.. code-block:: console

  Listing all versions of gene-fusions workflow.
  --------------------------------------------------------------------------------
  Workflow        Species Input
  gene-fusions    human   junctions
  gene-fusions    human   bams
  gene-fusions    human   fastqs
  --------------------------------------------------------------------------------

Users can find detailed information about each workflow by running:

.. code-block:: console

  $ raft.py available-workflows -w <WORKFLOW_NAME> -s <SPECIES> -i <INPUT_TYPE>

For example:

.. code-block:: console

  $ raft.py available-workflows -w gene-fusions -i fastqs

produces the output

.. code-block:: console

  Details for human gene-fusions workflow starting from fastqs.
  --------------------------------------------------------------------------------
  Default parameters:
  -------------------
  Parameter                       Value
  ---------                       -----
  fq_trim_tool                    fastp
  fq_trim_tool_parameters         None (tool defaults)
  fusion_tool                     starfusion
  fusion_tool_parameters          starfusion: --examine_coding_effect
  fusion_ref                      starfusion: ${params.ref_dir}/GRCh38_gencode_v37_CTAT_lib_Mar012021.plug-n-play
  dna_ref                         ${params.ref_dir}/Homo_sapiens_assembly38.fasta
  gtf                             None
  --------------------------------------------------------------------------------
  Workflow steps:
  ---------------
  utilities::parse_manifest
  fusion::manifest_to_fusions
  --------------------------------------------------------------------------------
  Alternate tools:
  ----------------
  Parameter                       Alternate tools
  ---------                       ---------------
  fq_trim_tool                    trim_galore,trimmomatic,fastp
  fusion_tool                     fusioncatcher,jstarfusion,starfusion,arriba


Running Off-the-shelf Workflows
______________________________

Users can run an off-the-shelf workflow with their own manifest by running:

.. code-block:: console

  $ raft.py run-ots -p <PROJECT_ID> \
                    -w <WORKFLOW_NAME> \
                    -s <SPECIES> \
                    -i <INPUT_TYPE> \
                    -m </PATH/TO/MANIFEST> 

For example:

.. code-block:: console

  $ raft.py run-ots -p my-project-tx-counts-01JAN2024 \
                    -w rna-alns-transript-counts \
                    -s human \
                    -i fastqs \
                    -m </PATH/TO/MANIFEST> 

Note that ``raft.py run-ots`` assumes human samples and FASTQs inputs, so the
the above command is equivilant to the following:

.. code-block:: console

  $ raft.py run-ots -p my-project-tx-counts-01JAN2024 \
                    -w rna-alns-transript-counts \
                    -m </PATH/TO/MANIFEST> 


Running Off-the-shelf Workflows with User-defined Parameters
____________________________________________________________
.. _userparams:

Users can run an off-the-shelf workflow with their preferred tools and tool
parameters using the ``raft.py`` ``-up/--user-params`` argument. This argument
can be specified any number of times. For example, if a user wants to run the
``rna-alns-transcript-counts`` workflow with their own manifest, but with
``trim_galore`` instead of ``fastp``, then they can run:

.. code-block:: console

  $ raft.py run-ots -p my-project-tx-counts-01JAN2024 \
                    -w rna-alns-transript-counts \
                    -m </PATH/TO/MANIFEST>  \
                    -up fq_trim_tool=fastp \
                    -up fq_trim_tool_parameters=fastp:'--paired'

Users may specify multiple tools and parameters for each of multiple tools using the following syntax:

.. code-block:: console

  $ raft.py run-ots -p ... \
                    ... \
                    -up fq_trim_tool=tool1,tool2 \
                    -up fq_trim_tool_parameters=tool1:'--params --for --tool 1',\
                                                tool2:'--params --for --tool 2'

Generally speaking, it is only recommended to use multiple tools for the `last`
modifying step of the workflow. For example, the ``gene-fusions`` workflow for
humans starting from FASTQs allow both ``fq_trim_tool`` and ``fusion_tool`` to
be specified. ``fusion_tool`` generates the ultimate outputs from the workflow
and thus users can specify multiple ``fusion_tool`` tools (e.g.
``fusion_tool=starfusion,arriba``). ``fq_trim_tool``, however, is upstream of
fusion calling and users should only specify a single tool for
``fq_trim_tool``.
