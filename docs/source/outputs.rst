Outputs
_______

RAFT outputs are available in a project's ``outputs/`` directory. The ``outputs/`` directory can contain a variety of subdirectories but generally includes at least ``qc/``, ``reports/``, and ``samples/``.

``reports/``
============
The ``reports/`` directory contains reports generated by Nextflow:

* dag.dot - A directed acyclic graph ``dot`` file that can be used to understand the flow among processes. The ``.dot`` file can be converted to a graph using ``graphviz``.

* report.html - A report with a wealth of information regarding each process run by the workflow. Useful for understanding resource limitations within the workflow.

* timeline.html - A report showing a graphical representation of each process's run time.

* trace.txt - A text file containing information about each process run. Useful for troubleshooting as process names can be ``grep``ed. This file exists within the project's ``log/`` directory while the workflow is running.

Hierarchical Output Directory Structure
=======================================

Directories containing sample-level outputs (``qc/`` and ``samples/``) follow:

.. code-block:: console

  samples/
    \__<DATASET>
          \__<PAT_NAME>
                \__<RUN_NAME>

Therefore, samples outputs are defined by the ``Dataset``, ``Pat_Name``, and ``Run_Name`` columns from the user-provided manifest.

Some situations, like variant calling, may involve multiple samples from the same patient. In this case, the ``<RUN_NAME>/`` directory is defined as ``<RUN_NAME_1>_<RUN_NAME2>/``.

``samples/``
============

Sample-level outputs can be found within the ``samples/`` directory. Outputs are partitioned by sample where appropriate. For example, the outputs for samples ``ar-279`` for patient ``Pt01`` in datasets ``Foo_2024`` can be found in ``outputs/samples/Foo_2024/Pt01/ar-279/``.

.. code-block:: console

  ls samples/Foo_2024/Pt01/ar-279/
  fastp
  salmon_aln_quant
  samtools_coverage
  seq2hla
  seqtk_subseq
  starfusion

Each subdirectory, (e.g. ``starfusion/``) contains symbolic links to the output files contained within the project's ``work/`` dirctory.

``qc/``
=======

Quality control data for each sample can be found in the ``qc/`` directory. These outputs are generally tool-level reported metrics.
