Installation
============

Prerequisites
______________
``raft`` requires that ``singularity`` (currently known as ``apptainer``) be installed. 
``singularity`` is used for downloading reference files.
Users may still use other containerization tools (e.g. ``docker``) within ``raft``.

Installing through Conda
________________________

To install RAFT using Conda, run:

.. code-block:: console

  $ conda install -c bioconda -c conda-forge -c raft reproducible-analyses-framework-and-tools

Installing through pip
______________________

To install RAFT using pip (recommmended), run:

.. code-block:: console

  $ pip install --user reproducible-analyses-framework-and-tools

.. note::
  ``pip`` will install ``raft`` to ``~/.local/bin``.
  Please ensure this directory is in your ``$PATH`` 
  (``export PATH=$PATH:~/.local/bin``).

Setup
_____

RAFT requires a directory consisting of various folders including:
``/Projects``, ``/References``, and ``/Metadata``. This directory also contains
the RAFT configuration file. To set up RAFT, run:

.. code-block:: console

  $ raft.py setup

This will prompt the user to provide paths for each require subdirectory while
providing a sensible defeault. This is useful for putting directories that will
containlarge files (e.g. ``/Projects``) on a storage device with sufficient
storage. The resulting subdirectory within the RAFT setup directory will be a
symlink to the user-specified directory.

Users that want the default setup in which all RAFT subdirectories are created in their current working directory can run:

.. code-block:: console

  $ raft.py setup -d
